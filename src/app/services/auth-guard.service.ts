//guard de l'Authentification
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';

//permet d'injecter le service "auth"
@Injectable()
export class AuthGuard implements CanActivate {

  //Permet d'utiliser le service auth et le service router
  constructor(private authService: AuthService, private router: Router){ }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.authService.isAuth){
        return true;
      } else {
        this.router.navigate(['/auth']);
      }
  }
}
