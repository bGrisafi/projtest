import { Subject } from 'rxjs/Subject';

export class AppareilService {

  //création d'un observable "subject" pour créer une couche d'abstraction
  //et ne pas travailler directement sur l'array appareils
  appareilsSubject = new Subject<any[]>();

  private appareils = [
    {
      id: 1,
      name: 'Machine à laver',
      status: 'éteint'
    },
    {
      id: 2,
      name: 'Frigo',
      status: 'allumé'
    },
    {
      id: 3,
      name: 'Ordinateur',
      status: 'éteint'
    }
  ];

  //gère l'émission des infos par le subject
  emitAppareilSubject() {
    this.appareilsSubject.next(this.appareils.slice());
  }

  //allume tout
  switchOnAll(){
    for(let appareil of this.appareils) {
      appareil.status = 'allumé';
    }
    this.emitAppareilSubject();
  }

  //éteint tout
  switchOffAll(){
    for(let appareil of this.appareils) {
      appareil.status = 'éteint';
    }
    this.emitAppareilSubject();
  }

  //allume un appareil selon l'index
  switchOnOne(i: number) {
    this.appareils[i].status = 'allumé';
    this.emitAppareilSubject();
  }

  //éteint un appareil selon l'index
  switchOffOne(i: number) {
    this.appareils[i].status = 'éteint';
    this.emitAppareilSubject();
  }
  //récupère un appareil par son id
  getAppareilById(id:number){
    /*const appareil = this.appareils.find(
      (s) => {
        return s.id === id;
      }
    );*/
    const appareil = this.appareils[id-1];
    return appareil;
  }

  addAppareil(name: string, status: string) {//ajoute un appareil au tableau
      const appareilObject = {
        id: 0,
        name: '',
        status: ''
      };
      appareilObject.name = name;
      appareilObject.status = status;
      appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
      this.appareils.push(appareilObject);
      this.emitAppareilSubject();
  }
}
