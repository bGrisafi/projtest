import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/interval';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  title = 'Projet test';

  constructor() {}

  secondes: number;
  counterSubscription: Subscription;
  ngOnInit(){
    //création d'un observable qui incrémente chaque seconde
    const counter = Observable.interval(1000);
    //subscription a l'observable pour l'utiliser
    this.counterSubscription = counter.subscribe(
      (value) =>{//récupère la valeur de l'observable en cours de fonctionnement
        this.secondes = value;
      },
      (error) =>{//retoune les erreurs de l'observable
        console.log('erreur: ' + error);
      },
      () =>{//retourne la fin d'execution de l'observable
        console.log('observable terminé');
      }
    );
  }

  ngOnDestroy(){
    //supprime la subscription a la destruction du component
    this.counterSubscription.unsubscribe();
  }
}
