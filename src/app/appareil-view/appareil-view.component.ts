import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.sass']
})
export class AppareilViewComponent implements OnInit {

  appareils: any[];
  appareilSubscription: Subscription;//subscription pour le subject gérant l'array d'appareils

  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });



  constructor(private appareilService: AppareilService) {}

  ngOnInit() {
    //hydratation de appareilSubscription
    this.appareilSubscription = this.appareilService.appareilsSubject.subscribe(
      (appareils: any[]) => {
        //hydratation de l'array appareils
        this.appareils = appareils;
      }
    );
    this.appareilService.emitAppareilSubject();
  }

    onAllumer(){
      this.appareilService.switchOnAll();
    }
    onEteindre(){
      if(confirm('Êtes-vous sur de vouloir tout éteindre ?')){
        this.appareilService.switchOffAll();
      }
    }
    ngOnDestroy() {//destruction de la subscription a la destruction du component
      this.appareilSubscription.unsubscribe();
    }

}
