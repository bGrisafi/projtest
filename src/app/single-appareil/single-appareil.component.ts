import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-appareil',
  templateUrl: './single-appareil.component.html',
  styleUrls: ['./single-appareil.component.sass']
})
export class SingleAppareilComponent implements OnInit {
  name: string = "Appareil";
  status: string = "éteint";
  constructor(private appareilService: AppareilService, private route: ActivatedRoute) { }

  ngOnInit() {
    //récupère l'id depuis la route
    const id = this.route.snapshot.params['id'];
    //récupère le nom et le statut de l'appareil dans le service par l'id
    this.name = this.appareilService.getAppareilById(+id).name;
    this.status = this.appareilService.getAppareilById(+id).status;
  }

}
