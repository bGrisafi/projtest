import { Component,Input, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service'
@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.sass']
})
export class AppareilComponent implements OnInit {

  //champ input
  @Input() appareilName: string;
  //statut de l'appareil
  @Input() appareilStatus: string;
  //index de l'appareil
  @Input() index: number;
  //id de l'appareil
  @Input() id: number;

  constructor(private appareilService: AppareilService) { }

  ngOnInit() {
  }

  getStatus() {
    return this.appareilStatus;
  }

  getColor(){
    if(this.appareilStatus === "éteint"){
      return 'red';
    }else {
      return 'green';
    }
  }

  onSwitch() {
    if(this.appareilStatus === 'allumé') {
      this.appareilService.switchOffOne(this.index);
    } else if(this.appareilStatus === 'éteint') {
      this.appareilService.switchOnOne(this.index);
    }
  }


}
